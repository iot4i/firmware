iot4i/firmware
========================

This repository contains the source code for the iot4i devices.

You need [PlatformIO](http://platformio.org) installed to build and run the program.

I recommend [PlatformIO IDE](http://platformio.org/platformio-ide) for developpement.

## Board
![iot4i-circuit-readme](https://gitlab.com/iot4i/firmware/uploads/105c6f39f96be03f06bc25fda503e620/iot4i-circuit-readme.png)

Detailed circuit on [circuits.io](https://circuits.io/circuits/4799737-iot4i-esp8266-ws2812b-capacitive-sensor#schematic)

## Environment variables
As the firmware must be built with the wifi ssid, the wifi key and the controlcenter server ip, you must set the build flags corresponding to your device configuration through the `PLATFORMIO_BUILD_FLAGS` env variable before running any command :
```bash
export PLATFORMIO_BUILD_FLAGS='-DWIFI_SSID=\"YOUR SSID\" -DWIFI_KEY=\"YOUR SECRET\" -DIOT_SERVER=\"192.168.1.XX\" -DLED_COUNT=50 -DPIN_LED=D1 -DPIN_CAPACITIVE_RECEIVE=D2 -DPIN_CAPACITIVE_SEND=D3'
```

* `PIN_CAPACITIVE_RECEIVE` and `DPIN_CAPACITIVE_SEND` are optional.
* Change the value of `CAPACITIVE_THRESHOLD` depending on your configuration.

## Build
```bash
pio run
```

## Upload on the device
If you have followed the [quickstart of PlatformIO](http://docs.platformio.org/en/latest/quickstart.html) or the [quickstart of PlatformIO IDE](http://docs.platformio.org/en/latest/ide/atom.html#installation) you can upload the binary to your device with the following command:
```bash
pio run --target upload
```
