FROM python:2.7

ENV PLATFORMIO_BUILD_FLAGS='-DWIFI_SSID=\"YOURSSID\" -DWIFI_KEY=\"YOURSECRET\" -DIOT_SERVER=\"192.168.1.XX\" -DLED_COUNT=50 -DPIN_LED=D1'

RUN pip install platformio==3.3.0 \
&& platformio platforms install espressif8266@1.3.0

WORKDIR /src

ADD . /src

RUN echo $PLATFORMIO_BUILD_FLAGS
RUN platformio run
