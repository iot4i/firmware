#include <stdlib.h>
#include <ESP8266WiFi.h>
#include <Adafruit_NeoPixel.h>
#include <ArduinoJson.h>
#include <CapacitiveSensor.h>

#ifndef PIN_LED
#define PIN_LED        D4
#endif
#ifndef PIN_CAPACITIVE_SEND
#define PIN_CAPACITIVE_SEND  -1
#endif
#ifndef PIN_CAPACITIVE_RECEIVE
#define PIN_CAPACITIVE_RECEIVE  -1
#endif
#ifndef CAPACITIVE_THRESHOLD
#define CAPACITIVE_THRESHOLD  50
#endif
#ifndef LED_COUNT
#define LED_COUNT      1
#endif
#define LOOP_DELAY     500
#define HTTP_TIMEOUT   5000

Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, PIN_LED, NEO_GRB + NEO_KHZ800);
CapacitiveSensor* capacitiveSensor;
const char* url = "/api/v1/color";
const int httpPort = 80;


void startCapacitive(int pinSend, int pinReceive){
        capacitiveSensor = new CapacitiveSensor(pinSend, pinReceive);
}

void setup() {
        Serial.begin(115200);
        delay(1000);

        // We start by connecting to a WiFi network
        Serial.printf("Connecting to %s\n", WIFI_SSID);

        /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
           would try to act as both a client and an access-point and could cause
           network-issues with your other WiFi-devices on your WiFi-network. */
        WiFi.mode(WIFI_STA);
        WiFi.begin(WIFI_SSID, WIFI_KEY);

        while (WiFi.status() != WL_CONNECTED) {
                delay(500);
                Serial.printf("Connecting to %s\n", WIFI_SSID);
        }

        Serial.printf("\nWiFi connected, IP address: %s\n",WiFi.localIP().toString().c_str());
        Serial.println(WiFi.localIP());

        // Wifi connected, we can start neopixel
        strip.begin();

        // Capacitive sensor
        if(PIN_CAPACITIVE_SEND != -1 && PIN_CAPACITIVE_RECEIVE != -1) {
                startCapacitive(PIN_CAPACITIVE_SEND,PIN_CAPACITIVE_RECEIVE);
        }
}

// String to long, skiping the first char that is '#'
int colorStringToInt(const char * color){
        return (int) strtol(&color[1], NULL, 16);
}

// Set a color for all leds
void setColorForAllLeds(Adafruit_NeoPixel* pixels, int color){
        int r = color >> 16;
        int g = color >> 8 & 0xFF;
        int b = color & 0xFF;

        Serial.printf("Setting led color to %d,%d,%d\n", r,g,b);

        int i;
        for(i=0; i<pixels->numPixels(); i++) {
                // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
                pixels->setPixelColor(i, pixels->Color(r,g,b));
        }
}

// Take an hexadecimal string forlatted like : #RRGGBB
void setColorForAllLeds(Adafruit_NeoPixel* pixels, const char* hexstring){
        int number = colorStringToInt(hexstring);
        setColorForAllLeds(pixels, number);
}



int getCapacitiveStatus(CapacitiveSensor* capacitiveSensor){
        if(PIN_CAPACITIVE_SEND != -1 && PIN_CAPACITIVE_RECEIVE != -1) {
                long capacitiveResult =  capacitiveSensor->capacitiveSensor(30);
                Serial.println(capacitiveResult);
                if(capacitiveResult > CAPACITIVE_THRESHOLD) {
                        return HIGH;
                }else {
                        return LOW;
                }
        }
        return LOW;
}

int getColorFromControlCenter(const char * host, const char * url){

        int color = 0;
        Serial.printf("HTTP: Connecting to %s\n", IOT_SERVER);

        // Use WiFiClient class to create TCP connections
        WiFiClient client;
        if (!client.connect(IOT_SERVER, httpPort)) {
                Serial.printf("HTTP: Connection to %s failed\n", IOT_SERVER);
                return color;
        }

        Serial.printf("HTTP: Requesting URL %s\n", url);

        // This will send the request to the server
        client.printf("GET %s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n",url, IOT_SERVER);
        unsigned long timeout = millis();
        while (client.available() == 0) {
                if (millis() - timeout > HTTP_TIMEOUT) {
                        Serial.println(">>> Client Timeout !");
                        client.stop();
                        return color;
                }
        }

        // Read the response until the last line.
        // We know that the last line in the response is the json
        String line;
        while(client.available()) {
                line = client.readStringUntil('\r');
        }
        if(line.length() == 0) {
                Serial.println("HTTP: Received empty string");
                return color;
        }

        Serial.printf("HTTP: Response received : %s\n",line.c_str());

        StaticJsonBuffer<200> jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(line.c_str());
        if (!root.success()) {
                Serial.printf("JSON: parseObject() failed: %s\n", line.c_str());
                return color;
        }

        const char *  colorString = root["hexa"];
        if(colorString == NULL) {
                Serial.printf("JSON: Unable to fetch json 'hexa' property' in: %s\n", line.c_str());
                return color;
        }
        Serial.printf("JSON: color parsed: %s\n",colorString);
        return colorStringToInt(colorString);
}


enum LED_STATE { AUTOMATIC, MANUAL, SWITCHED_TO_MANUAL };
LED_STATE currentLedState = AUTOMATIC;

LED_STATE getNewLedState(LED_STATE currentState, int manualStatus ){
        if(currentState == AUTOMATIC) {
                if(manualStatus == HIGH) {
                        return SWITCHED_TO_MANUAL;
                }
        }
        else if(currentState == MANUAL) {
                if(manualStatus == HIGH) {
                        return AUTOMATIC;
                }
        }else {
                return MANUAL;
        }
}

void loop() {
        delay(LOOP_DELAY);
        int manualStatus = getCapacitiveStatus(capacitiveSensor);
        currentLedState = getNewLedState(currentLedState,manualStatus);

        if(currentLedState == SWITCHED_TO_MANUAL) {
                setColorForAllLeds(&strip, "#FFFFFF");
        }
        else if (currentLedState == AUTOMATIC) {
                int color = getColorFromControlCenter(IOT_SERVER, url);
                setColorForAllLeds(&strip, color);
        }

        strip.show();
}
