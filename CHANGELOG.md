# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.2.0] - 2017-05-01
### Added
- capacitive sensor support

## [0.1.0] - 2017-04-24
### Added
- fetch color from controlcenter server
- send color to the led strip
